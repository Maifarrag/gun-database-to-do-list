const clear = document.querySelector(".clear");
const dateElement = document.getElementById("date");
var todos = Gun().get('todos');

// show the date
let options = { weekday: 'long', month: 'short', day: 'numeric' };
let today = new Date();
dateElement.innerHTML = today.toLocaleDateString('en-US', options);


      $('form').on('submit', function (event) {
        var input = $('form').find('input')
        todos.set({title: input.val()})
        input.val('')
        event.preventDefault()
      })

      todos.map().on(function (todo, id) {
        var li = $('#' + id)

        if (!li.get(0)) {
          li = $('<li>').attr('id', id ,'item').appendTo('ul');
          $("li").addClass("item");
        }
        if (todo) {

        var html = '<p  class="text"onclick="clickTitle(this)">' + todo.title + '</p>'
         //html = '<input  type="checkbox"  class="myClass" onclick="clickCheck(this)" ' + (todo.done ? 'checked' : '') + '>' +html;
         html= '<input type="checkbox"  onclick="clickCheck(this)"'+ (todo.done ? 'checked' : '') + '>' + html;
          html = '<i class="fa fa-trash-o de" onclick="clickDelete(this)" job="delete"></i>' + html;

          li.html(html)
        } else {
          li.remove()
        }
      })
      function clickTitle (element) {
        element = $(element)
        if (!element.find('input').get(0)) {
          element.html('<input value="' + element.html() + '" onkeyup="keypressTitle(this)">')
        }
      }

      function keypressTitle (element) {
        if (event.keyCode === 13) {
          todos.get($(element).parent().parent().attr('id')).put({title: $(element).val()})
        }
      }

      function clickCheck (element) {
        todos.get($(element).parent().attr('id')).put({done: $(element).prop('checked')})
      }

      function clickDelete (element) {
        todos.get($(element).parent().attr('id')).put(null)
      }

      // for refresh button
    clear.addEventListener('click',function(){
       localStorage.clear();
       location.reload();

    });